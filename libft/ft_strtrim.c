/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 16:20:17 by cpestour          #+#    #+#             */
/*   Updated: 2014/09/26 13:00:19 by verrk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *s)
{
	char	*new;
	char	*end;
	int		i;

	new = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1));
	end = (char *)s + (ft_strlen(s) - 1);
	i = 0;
	while (ft_isspace(*s))
		s++;
	while (ft_isspace(*end))
		end--;
	while (*s != *end)
	{
		new[i] = *s;
		i++;
		s++;
	}
	new[i] = '\0';
	return (new);
}
