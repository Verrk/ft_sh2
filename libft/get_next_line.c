/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getnextline.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 11:58:10 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/19 08:12:46 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				stock_buf(char *buf, char *line, int *pos, int *ok)
{
	static int	i = 0;

	while (buf[*pos])
	{
		if (buf[*pos] == '\n')
		{
			*ok = 1;
			(*pos)++;
			line[i] = '\0';
			i = 0;
			return (0);
		}
		else
		{
			line[i] = buf[*pos];
			i++;
			(*pos)++;
		}
	}
	if (buf[*pos] == '\0')
	{
		line[i] = '\0';
		*pos = 0;
	}
	return (1);
}

int				get_next_line(int const fd, char **line)
{
	static int	pos = 0;
	static char	tmp_buf[BUF_SIZE + 1];
	int			ok;
	char		*buf;
	int			ret;

	buf = (char *)malloc(sizeof(char) * (BUF_SIZE + 4096));
	ok = 0;
	while (!ok)
	{
		if (pos == 0)
		{
			if ((ret = read(fd, tmp_buf, BUF_SIZE)) < 0)
				return (-1);
			if (ret == 0)
			{
				*line = buf;
				return (0);
			}
			tmp_buf[ret] = '\0';
		}
		stock_buf(tmp_buf, buf, &pos, &ok);
	}
	*line = buf;
	return (1);
}
