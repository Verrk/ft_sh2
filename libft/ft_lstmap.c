/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 04:24:39 by verrk             #+#    #+#             */
/*   Updated: 2013/12/16 04:29:17 by verrk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list		*new;

	if ((new = (t_list *)malloc(sizeof(lst))) == NULL)
		return (NULL);
	while (lst)
	{
		new = f(lst);
		lst = lst->next;
	}
	return (lst);
}
