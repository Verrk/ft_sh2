/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 11:32:35 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/20 15:29:01 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh.h"

int				isbuiltin(char *arg, t_shell *shell)
{
	int			i;

	i = 0;
	while (shell->builtin[i])
	{
		if (ft_strcmp(arg, shell->builtin[i]) == 0)
			return (1);
		i++;
	}
	return (0);
}

int				ft_env(t_shell *shell, int fdout)
{
	int			i;

	i = 0;
	while (shell->env[i])
	{
		ft_putendl_fd(shell->env[i], fdout);
		i++;
	}
	return (0);
}

int				ft_hash(t_shell *shell, int fdout)
{
	t_hash		*tmp;

	tmp = shell->hash;
	while (tmp)
	{
		if (tmp->time > 0)
		{
			ft_putnbr_fd(tmp->time, fdout);
			ft_putchar_fd(' ', fdout);
			ft_putendl_fd(tmp->path, fdout);
		}
		tmp = tmp->next;
	}
	return (0);
}

int				builtin(char **arg, t_shell *shell, int fdout, int f)
{
	if (ft_strcmp(arg[0], "exit") == 0)
		exit_shell(&shell, arg);
	if (ft_strcmp(arg[0], "echo") == 0)
		shell->status = ft_echo(arg, shell, fdout);
	else if (ft_strcmp(arg[0], "cd") == 0)
		shell->status = ft_cd(&shell, arg);
	else if (ft_strcmp(arg[0], "env") == 0)
		shell->status = ft_env(shell, fdout);
	else if (ft_strcmp(arg[0], "hash") == 0)
		shell->status = ft_hash(shell, fdout);
	else if (ft_strcmp(arg[0], "setenv") == 0)
		shell->status = ft_checkenv(arg, &shell, 1);
	else if (ft_strcmp(arg[0], "unsetenv") == 0)
		shell->status = ft_checkenv(arg, &shell, 2);
	if (f == 0)
		exit(shell->status);
	return (shell->status);
}
