/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_cmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 14:13:37 by verrk             #+#    #+#             */
/*   Updated: 2015/04/28 12:46:06 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

int				get_fdin(t_cmdio fdin)
{
	int			fd;

	fd = -1;
	if (fdin.type == IO_DEF)
		return (STDIN_FILENO);
	if (fdin.type == IO_PIPE)
		return (fdin.pipenode->pfd[0]);
	if (fdin.type == IO_READ)
		fd = open(fdin.filename, O_RDONLY);
	return (fd);
}

int				get_fdout(t_cmdio fdout)
{
	int			fd;

	fd = -1;
	if (fdout.type == IO_DEF)
		return (STDOUT_FILENO);
	if (fdout.type == IO_PIPE)
		return (fdout.pipenode->pfd[1]);
	if (fdout.type == IO_TRUNC)
		fd = open(fdout.filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (fdout.type == IO_APPEND)
		fd = open(fdout.filename, O_CREAT | O_WRONLY | O_APPEND, 0644);
	if (fdout.type == IO_OUT)
		fd = fdout.fd;
	return (fd);
}

char			*get_path(char *arg, t_hash *hash)
{
	if (access(arg, F_OK) == 0)
	{
		while (hash)
		{
			if (ft_strcmp(hash->path, arg) == 0)
				hash->time++;
			hash = hash->next;
		}
		return (ft_strdup(arg));
	}
	else
	{
		while (hash)
		{
			if (ft_strcmp(hash->name, arg) == 0)
			{
				hash->time++;
				return (ft_strdup(hash->path));
			}
			hash = hash->next;
		}
		return (NULL);
	}
}

int				exec_cmd_path(t_ast *ast, t_shell *shell, int f, int fdout)
{
	int			fdin;
	pid_t		pid;

	pid = 0;
	fdin = get_fdin(ast->cmd->fdin);
	ast->cmd->path = get_path(ast->cmd->argv[0], shell->hash);
	if (ast->cmd->path)
	{
		if (f)
			pid = fork();
		if (pid == 0)
			exec_cmd_child(ast->cmd, shell, fdin, fdout);
		if (pid > 0)
			wait(&shell->status);
	}
	else
	{
		ft_putstr_fd("42sh: command not found: ", 2);
		ft_putendl_fd(ast->cmd->argv[0], 2);
		shell->status = 127;
		if (!f)
			exit_shell(&shell, NULL);
	}
	return (shell->status);
}

int				exec_cmd(t_ast *ast, t_shell *shell, int f)
{
	int			fdout;

	fdout = get_fdout(ast->cmd->fdout);
	if (isbuiltin(ast->cmd->argv[0], shell))
		return (builtin(ast->cmd->argv, shell, fdout, f));
	else
		return (exec_cmd_path(ast, shell, f, fdout));
	return (127);
}
