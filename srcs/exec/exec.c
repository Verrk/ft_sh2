/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 14:07:36 by verrk             #+#    #+#             */
/*   Updated: 2015/02/03 20:31:32 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

int				exec_semi(t_ast *ast, t_shell *shell)
{
	int			ret;

	if (ast->left)
		ret = exec(ast->left, shell, 1);
	if (ast->right)
		ret = exec(ast->right, shell, 1);
	return (ret);
}

int				exec_and_if(t_ast *ast, t_shell *shell)
{
	int			ret;

	ret = exec(ast->left, shell, 1);
	if (ret == 0)
		ret = exec(ast->right, shell, 1);
	return (ret);
}

int				exec_or_if(t_ast *ast, t_shell *shell)
{
	int			ret;

	ret = exec(ast->left, shell, 1);
	if (ret != 0)
		ret = exec(ast->right, shell, 1);
	return (ret);
}

int				exec_pipe(t_ast *ast, t_shell *shell, int f)
{
	pid_t		pid;
	int			ret;

	pid = 0;
	ret = 0;
	if (f)
		pid = fork();
	if (pid == 0)
		exec_pipe_child(ast, shell, &ret);
	else
		wait(&ret);
	return (ret);
}

int				exec(t_ast *ast, t_shell *shell, int f)
{
	if (ast->type == A_SEMI)
		shell->status = exec_semi(ast, shell);
	if (ast->type == A_AND_IF)
		shell->status = exec_and_if(ast, shell);
	if (ast->type == A_OR_IF)
		shell->status = exec_or_if(ast, shell);
	if (ast->type == A_PIPE)
		shell->status = exec_pipe(ast, shell, f);
	if (ast->type == A_CMD)
		shell->status = exec_cmd(ast, shell, f);
	return (shell->status);
}
