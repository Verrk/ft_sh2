/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 14:31:53 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/20 14:32:22 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

void			lexer_delone(t_token **token, t_token *tok)
{
	if (*token == tok)
		*token = tok->next;
	if (tok->prev)
		tok->prev->next = tok->next;
	if (tok->next)
		tok->next->prev = tok->prev;
	free(tok->tok);
	free(tok);
}

void			free_token(t_token **list)
{
	t_token		*tmp;

	while (*list)
	{
		tmp = *list;
		*list = tmp->next;
		free(tmp->tok);
		free(tmp);
	}
}
