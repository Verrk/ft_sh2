/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 17:13:07 by cpestour          #+#    #+#             */
/*   Updated: 2014/11/11 16:17:57 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

t_ast			*ast_create_node(t_ast *parent, t_dir dir, t_asttype type)
{
	t_ast		*new;

	new = (t_ast *)malloc(sizeof(t_ast));
	new->cmd = NULL;
	new->pfd = NULL;
	new->fd = -1;
	new->type = type;
	new->parent = parent;
	new->left = NULL;
	new->right = NULL;
	if (dir == DIR_LEFT)
		new->parent->left = new;
	if (dir == DIR_RIGHT)
		new->parent->right = new;
	return (new);
}

void			ast_cmd_init_fd(t_cmdio *fd)
{
	fd->type = IO_DEF;
	fd->pipenode = NULL;
	fd->filename = NULL;
}

t_ast			*ast_create_cmd_node(t_ast *parent, t_dir dir)
{
	t_ast		*node;
	t_cmd		*cmd;

	node = ast_create_node(parent, dir, A_CMD);
	cmd = (t_cmd *)malloc(sizeof(t_cmd));
	cmd->path = NULL;
	cmd->argv = NULL;
	ast_cmd_init_fd(&cmd->fdin);
	ast_cmd_init_fd(&cmd->fdout);
	node->cmd = cmd;
	return (node);
}
