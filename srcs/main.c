/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 07:44:38 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/19 08:58:34 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

void				init_shell2(t_shell *shell)
{
	shell->env = (char **)malloc(sizeof(char *) * 4);
	shell->path = NULL;
	shell->pwd = getcwd(NULL, 0);
	shell->env[0] = ft_strnew(ft_strlen(shell->pwd) + 4);
	ft_strcat(shell->env[0], "PWD=");
	ft_strcat(shell->env[0], shell->pwd);
	shell->old_pwd = ft_strdup(shell->pwd);
	shell->env[1] = ft_strnew(ft_strlen(shell->old_pwd) + 7);
	ft_strcat(shell->env[1], "OLDPWD=");
	ft_strcat(shell->env[1], shell->old_pwd);
	shell->home = ft_strdup("/home/verrk");
	shell->env[2] = ft_strnew(ft_strlen(shell->home) + 5);
	ft_strcat(shell->env[2], "HOME=");
	ft_strcat(shell->env[2], shell->home);
	shell->env[3] = NULL;
}

void				init_shell(t_shell *shell, char **env)
{
	int				i;

	if (!env[0])
	{
		init_shell2(shell);
		return ;
	}
	i = -1;
	shell->env = (char **)malloc(sizeof(char *) * (ft_strlen_tab(env) + 1));
	while (env[++i])
		shell->env[i] = ft_strdup(env[i]);
	shell->env[i] = NULL;
	i = -1;
	while (shell->env[++i])
	{
		if (ft_strncmp(shell->env[i], "PATH=", 5) == 0)
			shell->path = ft_strdup(&(shell->env[i][5]));
		if (ft_strncmp(shell->env[i], "PWD=", 4) == 0)
			shell->pwd = ft_strdup(&(shell->env[i][4]));
		if (ft_strncmp(shell->env[i], "OLDPWD=", 7) == 0)
			shell->old_pwd = ft_strdup(&(shell->env[i][7]));
		if (ft_strncmp(shell->env[i], "HOME=", 5) == 0)
			shell->home = ft_strdup(&(shell->env[i][5]));
	}
}

void				init_builtin(t_shell *shell)
{
	shell->builtin[0] = ft_strdup("cd");
	shell->builtin[1] = ft_strdup("env");
	shell->builtin[2] = ft_strdup("setenv");
	shell->builtin[3] = ft_strdup("unsetenv");
	shell->builtin[4] = ft_strdup("hash");
	shell->builtin[5] = ft_strdup("echo");
	shell->builtin[6] = ft_strdup("exit");
	shell->builtin[7] = NULL;
}

void				get_hash(t_shell *shell)
{
	char			**path_table;
	DIR				*dir;
	struct dirent	*d;
	int				i;

	if (shell->path)
	{
		path_table = ft_strsplit(shell->path, ':');
		i = 0;
		while (path_table[i])
		{
			if ((dir = opendir(path_table[i])) != NULL)
			{
				while ((d = readdir(dir)) != NULL)
				{
					if (d->d_name[0] != '.')
						push(&(shell->hash), d->d_name, path_table[i]);
				}
				closedir(dir);
			}
			free(path_table[i]);
			i++;
		}
		free(path_table);
	}
}

int					main(void)
{
	extern char		**environ;
	t_shell			*shell;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->hash = NULL;
	shell->token = NULL;
	shell->ast = NULL;
	init_shell(shell, environ);
	init_builtin(shell);
	get_hash(shell);
	shell->status = 0;
	prompt(shell);
	return (0);
}
