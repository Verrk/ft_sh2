#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/10/07 07:40:15 by cpestour          #+#    #+#              #
#    Updated: 2015/05/19 08:59:26 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC=gcc
NAME=ft_minishell2
CFLAGS=-Wall -Werror -Wextra -Ilibft/includes -Iincludes -g
LDFLAGS=-Llibft -lft
SRC=srcs/main.c srcs/prompt.c \
	srcs/builtin/hash.c srcs/builtin/cd.c srcs/builtin/env.c \
	srcs/builtin/exit.c srcs/builtin/builtin.c srcs/builtin/echo.c \
	srcs/lexer/token.c srcs/lexer/utils.c srcs/lexer/error.c srcs/lexer/free.c \
	srcs/ast/ast.c srcs/ast/utils.c srcs/ast/ast_cmd.c \
	srcs/exec/exec.c srcs/exec/exec_cmd.c srcs/exec/child.c
OBJ=$(SRC:.c=.o)

all: lib $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

lib:
	make -C libft

clean:
	rm -f $(OBJ) *~ srcs/*~ srcs/lexer/*~ srcs/ast/*~ srcs/exec/*~ srcs/builtin/*~ includes/*~
	make clean -C libft

fclean: clean
	rm -f $(NAME)
	make fclean -C libft

re: fclean all
